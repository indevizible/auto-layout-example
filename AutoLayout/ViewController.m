//
//  ViewController.m
//  AutoLayout
//
//  Created by Nattawut Singhchai on 9/16/14.
//  Copyright (c) 2014 Nattawut Singhchai. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *firstLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstLabelWidthConstraint;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self resize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)resize
{
    _firstLabelWidthConstraint.constant = [_firstLabel.attributedText boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, _firstLabel.frame.size.height) options:NSStringDrawingUsesFontLeading context:nil].size.width + 1;
    [_firstLabel layoutIfNeeded];
}

- (IBAction)valChanged:(UITextField *)sender {
    self.firstLabel.attributedText = [[NSAttributedString alloc] initWithString:sender.text attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17]}];
    [self resize];
}

@end
