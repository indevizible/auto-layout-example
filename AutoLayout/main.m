//
//  main.m
//  AutoLayout
//
//  Created by Nattawut Singhchai on 9/16/14.
//  Copyright (c) 2014 Nattawut Singhchai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
